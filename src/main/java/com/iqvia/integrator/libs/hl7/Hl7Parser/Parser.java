package com.iqvia.integrator.libs.hl7.Hl7Parser;

import com.iqvia.integrator.libs.hl7.Model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@ComponentScan("Hl7ParserConfig")
public class Parser {
    JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);

    public Parser() throws JAXBException {
    }

    public String toXmlString(Message hl7Message) throws JAXBException {
        //JAXBContext jaxbContext = JAXBContext.newInstance(Message.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(hl7Message, sw);
        String xmlString = sw.toString();
        return xmlString;
    }

    public String toHl7String(Message hl7Message) {
        StringBuilder sb = new StringBuilder();
        //iterate segments
        Iterator<Segment> iteratorSegment = hl7Message.getSegments().iterator();
        while (iteratorSegment.hasNext()) {
            Segment segment = iteratorSegment.next();
            sb.append(segment.getName());
            segment.getFields().forEach(field -> {
                sb.append("|");
                if (field.getSubFields() != null) {
                    Iterator<Field> iteratorSubField = field.getSubFields().iterator();
                    //iterate subFields in field [array of fields]
                    while (iteratorSubField.hasNext()) {
                        Field currentSubField = iteratorSubField.next();
                        //iterate components in subField
                        if (currentSubField.getComponents() != null) {
                            Iterator<Component> iteratorSubFieldComponent = currentSubField.getComponents().iterator();
                            iterrateComponents(sb, iteratorSubFieldComponent);
                        }   //get direct value
                        else if (currentSubField.getValue() != null) {
                            sb.append(currentSubField.getValue());
                        }
                        if (iteratorSubField.hasNext()) {
                            sb.append("~");
                        }
                    }
                }  //iterate field
                else if (field.getComponents() != null) {
                    //iterate component
                    Iterator<Component> iteratorComponent = field.getComponents().iterator();
                    iterrateComponents(sb, iteratorComponent);
                } // get direct value
                 else if (field.getValue() != null) {
                    sb.append(field.getValue());
                }
            });
            if (iteratorSegment.hasNext()) {
                sb.append("\r\n");
            }
        }

        return sb.toString();
    }

    public Message fromHl7String(String hl7String) {
        //check if hl7 lines are properly separated
        if (!hl7String.contains("\r")) {
            hl7String = hl7String.replaceAll("\n", "\r\n");
        }
        //get hl7 segments from string
        String[] segments = hl7String.split("\r\n");
        //reading hl7 segments
        List<Segment> hl7segments = new ArrayList<>();
        parseHl7Segment(segments, hl7segments);
        Message hl7Message = new Message();
        hl7Message.setSegments(hl7segments);
        return hl7Message;
    }

    public Message fromXmlString(String xmlString) throws JAXBException {
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(xmlString);
        Message myMessage = (Message) jaxbUnmarshaller.unmarshal(reader);
        return myMessage;
    }

    private void iterrateComponents(StringBuilder sb, Iterator<Component> iteratorComponent) {
        while (iteratorComponent.hasNext()) {
            Component currentComponent = iteratorComponent.next();
            if (currentComponent.getValue() != null) {
                sb.append(currentComponent.getValue());
            } else if (currentComponent.getSubComponents() != null) {
                Iterator<SubComponent> iteratorSubComponent = currentComponent.getSubComponents().iterator();
                while (iteratorSubComponent.hasNext()) {
                    SubComponent currentSubComponent = iteratorSubComponent.next();
                    if (currentSubComponent.getValue() != null) {
                        sb.append(currentSubComponent.getValue());
                    }
                    if (iteratorSubComponent.hasNext()) {
                        sb.append("&");
                    }
                }
            }
            if (iteratorComponent.hasNext()) {
                sb.append("^");
            }
        }
    }

    private void parseHl7Field(String field, Field hl7Field) {
        //check if contains field
        if (field.contains("^")) {
            String[] components = field.split("\\^");
            List<Component> hl7Components = new ArrayList<>();
            for (int j = 0; j < components.length; j++) {
                String component = components[j];
                Component hl7Component = new Component();
                hl7Component.setIndex(j + 1);
                if (component.contains(("&"))) {
                    List<SubComponent> hl7SupSubComponents = new ArrayList<>();
                    //split by & to get sub fields
                    String[] subComponents = component.split("&");
                    for (int k = 0; k < subComponents.length; k++) {
                        String subComponent = subComponents[k];
                        SubComponent hl7SubComponent = new SubComponent();
                        hl7SubComponent.setIndex(k + 1);
                        if (!subComponent.isEmpty()) {
                            hl7SubComponent.setValue(subComponent);
                        }
                        hl7SupSubComponents.add(hl7SubComponent);
                    }
                    hl7Component.setSubComponents(hl7SupSubComponents);
                } else {
                    if (!component.isEmpty()) {
                        hl7Component.setValue(component);
                    }
                }
                hl7Components.add(hl7Component);
            }
            hl7Field.setComponents(hl7Components);
        } else {
            if (!field.isEmpty()) {
                hl7Field.setValue(field);
            }
        }
    }

    private void parseHl7Segment(String[] segments, List<Segment> hl7segments) {
        for (int p = 0; p < segments.length; p++) {
            String segment = segments[p];
            Segment hl7segment = new Segment();
            hl7segment.setIndex(p + 1);
            hl7segment.setName(segment.substring(0, segment.indexOf("|")));
            //split by | to get fields
            String[] fields = segment.split("\\|");
            List<Field> hl7Fields = new ArrayList<>();
            for (int i = 1; i < fields.length; i++) {
                String field = fields[i];
                Field hl7Field = new Field();
                hl7Field.setIndex(i);
                //ignore separators for first for MSH Segment field [this field contains separators as value]
                if (!(i == 1 && hl7segment.getName().equals("MSH"))) {
                    if (field.contains("~")) {
                        //split by ~ to get array fields
                        String[] subFields = field.split("~");
                        List<Field> hl7SubFields = new ArrayList<>();
                        //split by ^ to get components
                        for (int n = 0; n < subFields.length; n++) {
                            String subField = subFields[n];
                            Field hl7SubField = new Field();
                            hl7SubField.setIndex(n + 1);
                            parseHl7Field(subField, hl7SubField);
                            hl7SubFields.add(hl7SubField);
                        }
                        hl7Field.setSubFields(hl7SubFields);
                    } else {
                        parseHl7Field(field, hl7Field);
                    }
                } else {
                    hl7Field.setValue(field);
                }
                hl7Fields.add(hl7Field);
                hl7segment.setFields(hl7Fields);
            }

            hl7segments.add(hl7segment);
        }
    }

}
