package com.iqvia.integrator.libs.hl7.Model;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.List;

public class Field {
    String value;
    Integer index;
    List<Field> subFields;
    List<Component> components;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    @XmlAttribute(name = "index")
    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public List<Field> getSubFields() {
        return subFields;
    }

    public void setSubFields(List<Field> subFields) {
        this.subFields = subFields;
    }
}