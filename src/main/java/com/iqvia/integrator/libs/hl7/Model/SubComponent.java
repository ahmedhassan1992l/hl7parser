package com.iqvia.integrator.libs.hl7.Model;

import javax.xml.bind.annotation.XmlAttribute;

public class SubComponent {
    String value;
    Integer index;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @XmlAttribute(name = "index")
    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
