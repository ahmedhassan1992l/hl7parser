package com.iqvia.integrator.libs.hl7.Model;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.List;

public class Segment {

    Integer index;

    String name;

    List<Field> fields;

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    @XmlAttribute(name = "index")
    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    @XmlAttribute(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}