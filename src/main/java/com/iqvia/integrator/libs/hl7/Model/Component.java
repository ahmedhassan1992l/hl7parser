package com.iqvia.integrator.libs.hl7.Model;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.List;

public class Component {
    String value;
    Integer index;
    List<SubComponent> subComponents;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<SubComponent> getSubComponents() {
        return subComponents;
    }

    public void setSubComponents(List<SubComponent> subComponents) {
        this.subComponents = subComponents;
    }

    @XmlAttribute(name = "index")
    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}