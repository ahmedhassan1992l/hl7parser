package com.iqvia.integrator.libs.hl7;

import com.iqvia.integrator.libs.hl7.Hl7Parser.Parser;
import com.iqvia.integrator.libs.hl7.Model.Message;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

@SpringBootTest
@SpringBootConfiguration
class Hl7ParserApplicationTests {
	@Test

	void contextLoads() {
		Parser parser = null;
		try {
			parser = new Parser();
			String hl7String = "MSH|^~\\&|GHH LAB|ELAB-3|GHH OE|BLDG4|200202150930||ORU^R01|CNTRL-3456|P|2.4\n" +
					"EVN|dsd||||||||||||||||||z\n" +
					"PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320~~dd|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292|||||||AC555444444||67-A4335^OH^20030520\n" +
					"OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\n" +
					"OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|fff^&70_105|H|||F";
			Message hl7Message = parser.fromHl7String(hl7String);

			String xmlMessage = parser.toXmlString(hl7Message);

			Message hl7MessageFromXml = parser.fromXmlString(xmlMessage);
			String hl7StringFromXml = parser.toHl7String(hl7MessageFromXml);

			System.out.println("done!");
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

}
